import React from "react";
import Header from "../../component/Header";

const About = () => {
  return (
    <>
      <Header />
      <div className={"root"}>
        <h2>About</h2>
        <div>
          The site can be treated as the entry point for resources that point to
          animals help resources
        </div>
        <p></p>
        <p></p>
        <div>
          Ціль сайту - зібрати основні ресурси по допомогі тваринам в Україні.
        </div>
        <p></p>
        <p>
          Для розміщення контактів, будь ласка, пишіть на quadr988@outlook.com
        </p>
      </div>
    </>
  );
};

export default About;
