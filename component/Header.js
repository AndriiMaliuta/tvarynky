import Link from "next/link";
import React from "react";

const Header = () => {
  return (
    <div className="header">
      <div className="header-elem">
        <Link href="/">Home</Link>
      </div>
      <div className="header-elem">
        <Link href={"/about"}>About</Link>
      </div>
      {/* <div className='header-elem'>About</div> */}
    </div>
  );
};

export default Header;
